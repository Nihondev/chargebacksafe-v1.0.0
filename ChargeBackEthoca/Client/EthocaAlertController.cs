﻿using ChargeBackEthoca.Models.Dao;
using ChargeBackEthoca.Models.Dto;
using ChargeBackEthoca.Repository;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace ChargeBackEthoca.Client
{
    public class EthocaAlertController : ApiController
    {
        public dynamic JsonResult { get; set; }
        [Authentication]
        [HttpPost]
        public IHttpActionResult Post(EthocaDto JsonObj) 
        {
            try
            {
                //JObject enCode = JObject.Parse(JsonObj.ToString());
                string req = EthocaDao.Instance.InsDataResponse(JsonObj, "log");
                if (req == "OK")
                {
                    REQ_RESPOSE Response = new REQ_RESPOSE()
                    {
                        AppId = "ChareBackV1.0",
                        RESULT = "Success",
                        RESULT_CODE = "00200",
                        Version = "v1.0",
                        TimeStamp = DateTime.Now.ToString()
                    };
                    string jsonRetrun = JsonConvert.SerializeObject(Response, Formatting.None, new JsonSerializerSettings
                    {
                        NullValueHandling = NullValueHandling.Ignore
                    });
                    JsonResult = JsonConvert.DeserializeObject<dynamic>(jsonRetrun);
                }
            }
            catch(Exception ex)
            {
                REQ_RESPOSE Response = new REQ_RESPOSE()
                {
                    RESULT = ex.Message.ToString(),
                    RESULT_CODE = "00040",
                };
                string jsonRetrun = JsonConvert.SerializeObject(Response, Formatting.None, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                JsonResult = JsonConvert.DeserializeObject<dynamic>(jsonRetrun);
            }
            return Ok(JsonResult);
        }
	}
}
