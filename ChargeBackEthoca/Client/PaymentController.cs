﻿using ChargeBackEthoca.Models.Component;
using ChargeBackEthoca.Models.Dao;
using ChargeBackEthoca.Models.Dto;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Stripe;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace ChargeBackEthoca.Client
{
    public class PaymentController : ApiController
	{
		public dynamic JsonResult { get; set; }
		[HttpPost]
		public async Task<IHttpActionResult> BillingPortal(BillingDto req)
		{
			try
			{
				StripeConfiguration.ApiKey = ConfigurationManager.AppSettings["Private-key"];
				ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
				ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;

				var request = (HttpWebRequest)WebRequest.Create("https://api.stripe.com/v1/billing_portal/sessions");
				request.Method = "POST";
				request.ContentType = "application/json";

				var options = new Stripe.BillingPortal.SessionCreateOptions
				{
					//Customer = ChkUse.CustomerId,
					Customer = req.CustomerId,
					ReturnUrl = req.ReturnUrl,
				};
				var service = new Stripe.BillingPortal.SessionService();
				var session = await service.CreateAsync(options);
				PaymentDto i = new PaymentDto();
				i.Url = session.Url;
				i.objectBilling = session.Object;
				i.create = session.Created;
				i.customer = session.Customer;
				i.liveMode = session.Livemode;
				i.returnUrl = session.ReturnUrl;
				return Ok(new
				{
					url = "<a href='" + session.Url + "'>MANAGE BILLING</a>"
				});

			}
			catch (StripeException ex)
			{
				REQ_RESPOSE Response = new REQ_RESPOSE()
				{
					RESULT = ex.Message.ToString(),
					RESULT_CODE = "00040",
				};
				string jsonRetrun = JsonConvert.SerializeObject(Response, Formatting.None, new JsonSerializerSettings
				{
					NullValueHandling = NullValueHandling.Ignore
				});
				JsonResult = JsonConvert.DeserializeObject<dynamic>(jsonRetrun);
			}
			return JsonResult;
		}

		[HttpGet]
		public  IHttpActionResult GetBilling([FromUri] BillingDto Ref) //string CustomerId,string Status   
		{
			try
			{
				StripeConfiguration.ApiKey = ConfigurationManager.AppSettings["Private-key"];
				ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
				ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;

				var request = (HttpWebRequest)WebRequest.Create("https://api.stripe.com/v1/invoices");
				request.Method = "GET";
				request.ContentType = "application/json";

                var options = new InvoiceListOptions
                {
					//Limit = 3,
					Customer = Ref.CustomerId,
                    Status = Ref.Status,
					//Subscription = si_HdzjPAzfOmP7Lz,
				};
				var service = new InvoiceService();
				StripeList<Invoice> invoices = service.List(options);
				return Ok(invoices);
			}
            catch (StripeException ex)
            {
				REQ_RESPOSE Response = new REQ_RESPOSE()
				{
					RESULT = ex.Message.ToString(),
					RESULT_CODE = "00040",
				};
				string jsonRetrun = JsonConvert.SerializeObject(Response, Formatting.None, new JsonSerializerSettings
				{
					NullValueHandling = NullValueHandling.Ignore
				});
				JsonResult = JsonConvert.DeserializeObject<dynamic>(jsonRetrun);
			}
			return JsonResult;
		}
	}
}
