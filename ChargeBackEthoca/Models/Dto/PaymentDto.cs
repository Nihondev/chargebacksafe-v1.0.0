﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChargeBackEthoca.Models.Dto
{
    public class PaymentDto
    {
        public int ID { get; set; }
        public string Url { get; set; }
        public string objectBilling { get; set; }
        public DateTime create { get; set; }
        public string customer { get; set; }
        public Boolean liveMode { get; set; }
        public string returnUrl { get; set; }
        public string StripeResponse { get; set; }
        public string RawJObject { get; set; }
    }
}