﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChargeBackEthoca.Models.Dto
{
    public class UserDto
    {
        public int ID { get; set; }
        public string FIRST_NAME { get; set; }
        public string LAST_NAME { get; set; }
        public int MEMBER_ID { get; set; }
        public string USERNAME { get; set; }
        public string PASSWORD { get; set; }
        public string ADDRESS { get; set; }
        public string PHONE { get; set; }
        public string eMAIL { get; set; }
        public string CustomerId { get; set; }
        public string CRAETE_DATE { get; set; }
    }

    public class UserAuth
    {
        public string CustomerId { get; set; }
    }
}