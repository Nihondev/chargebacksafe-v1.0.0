﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChargeBackEthoca.Models.Dto
{
    public class SetUpDto
    {
        public int ID { get; set; }
        public string MID { get; set; }
        public string MIDAlias { get; set; }
        public string REMARK { get; set; }
        public int STATUS { get; set; }
        public string CREATE_DATE { get; set; }
        public string DESCRIPTOR { get; set; }
    }


}