﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChargeBackEthoca.Models.Dto
{
    public class ErrorResponseDto
    {
        public ErrorMessageDto ErrorMessage { get; set; }
    }
}