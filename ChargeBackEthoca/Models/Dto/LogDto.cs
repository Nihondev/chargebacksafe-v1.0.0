﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChargeBackEthoca.Models.Dto
{
    public class LogDto
    {
        public int ID { get; set; }
        public string IP { get; set; }
        public string BROWSER { get; set; }
        public string URL { get; set; }
        public string SYSTEM_OS { get; set; }
        public string CONNECTION_HARDWARE { get; set; }
        public string DATE { get; set; }
    }
}