﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChargeBackEthoca.Models.Dto
{
    public class ErrorMessageDto
    {
        public string Message { get; set; }
        public string Status { get; set; }
    
    }
}