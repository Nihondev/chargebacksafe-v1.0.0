﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChargeBackEthoca.Models.Dto
{
    public class EUser
    {
        public int MEMBER_ID { get; set; }
        public string USERIP { get; set; }
        public string USERID { get; set; }
        public DateTime LOGIN { get; set; }
        public string USERNAME { get; set; }
        public string CustomerId { get; set; }
        public string PASS { get; set; }
        public string TimeZone { get; set; }
    }
}