﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChargeBackEthoca.Models.Dto
{
    public class EmailDto
    {
        public int ID { get; set; }
        public string EMAIL_ALERT { get; set; }
        public string SUMMARY { get; set; }
        public string CONTENT { get; set; }
    }
}