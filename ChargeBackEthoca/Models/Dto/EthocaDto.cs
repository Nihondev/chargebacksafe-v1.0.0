﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChargeBackEthoca.Models.Dto
{
    public class EthocaDto
    {
        public int ROW_ID { get; set; }
        public string ethoca_id { get; set; }
        public string RESPOND { get; set; }
        public string type { get; set; }
        public string alert_timestamp { get; set; }
        public string TIME_RECIPE { get; set; }
        public string EXD { get; set; }
        public string age { get; set; }
        public string issuer { get; set; }
        public string card_number { get; set; }
        public string card_bin { get; set; }
        public string card_last4 { get; set; }
        public string transaction_timestamp { get; set; }
        public string merchant_descriptor { get; set; }
        public int member_id { get; set; }
        public string mcc { get; set; }
        public decimal amount { get; set; }
        public string currency { get; set; }
        public string transaction_type { get; set; }
        public string initiated_by { get; set; }
        public string is_3d_secure { get; set; }
        public string source { get; set; }
        public string auth_code { get; set; }
        public string merchant_member_name { get; set; }
        public string arn { get; set; }
        public string transaction_id { get; set; }
        public string chargeback_reason_code { get; set; }
        public decimal chargeback_amount { get; set; }
        public string chargeback_currency { get; set; }
    }

    public class REQ_RESPOSE
    {
        public string STATUS { get; set; }
        public string AppId { get; set; }
        public int ID { get; set; }
        public string RESULT { get; set; }
        public string Version { get; set; }
        public string TimeStamp { get; set; }
        public string url { get; set; }
        public string RESULT_CODE { get; set; }
    }
}