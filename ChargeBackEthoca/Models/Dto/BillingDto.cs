﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ChargeBackEthoca.Models.Dto
{
    public class BillingDto
    {
        [Required]
        public string ReturnUrl { get; set; }
        [Required]
        public string CustomerId { get; set; }
        [Required]
        public string Status { get; set; }
        public string username { get; set; }
        public string password { get; set; }

        public IEnumerable<BillingDto> objBillingDto { get; set; }
    }
}