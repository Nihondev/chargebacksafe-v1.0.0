﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Web;

namespace ChargeBackEthoca.Models.Component
{
    public class Info
    {
        public static string GetHardware()
        {
            string hardware = string.Empty;
            ManagementObjectSearcher myOperativeSystemObject = new ManagementObjectSearcher("select * from Win32_OperatingSystem");

            foreach (ManagementObject obj in myOperativeSystemObject.Get())
            {
                hardware = obj["Caption"].ToString();
            }
            return hardware;

        }
        public static string GetOsName(OperatingSystem os_info)
        {
            string version =
                os_info.Version.Major.ToString() + "." +
                os_info.Version.Minor.ToString();
            switch (version)
            {
                case "10.0": return "Windows 10/Server 2016";
                case "6.3": return "Windows 8.1/Server 2012 R2";
                case "6.2": return "Windows 8/Server 2012";
                case "6.1": return "Windows 7/Server 2008 R2";
                case "6.0": return "Windows Server 2008/Vista";
                case "5.2": return "Windows Server 2003 R2/Server 2003/XP 64-Bit Edition";
                case "5.1": return "Windows XP";
                case "5.0": return "Windows 2000";
            }
            return "Unknown";
        }

    }
}