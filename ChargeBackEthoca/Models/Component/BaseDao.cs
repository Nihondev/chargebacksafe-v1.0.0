﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ChargeBackEthoca.Models.Component
{
    public class BaseDao<T> where T : new()
    {
        private static T singleton = new T();
        protected int CommandTimeoutSecond = 3600;

        public static T Instance
        {
            get
            {
                return singleton;
            }
        }

        protected virtual System.Xml.XmlDocument SqlDocument
        {
            get
            {
                return null;
            }
        }

        #region Connection
        protected SqlConnection CreateConnection()
        {
            string constr = ConfigurationManager.AppSettings["Constr"];
            //string dbpass = ConfigurationManager.AppSettings["DBPASS"];
            //constr = constr + dbpass;
            SqlConnection con = new SqlConnection(constr);
            return con;
        }

        protected void AddSQLParam(SqlParameterCollection @params, string name, object val)
        {
            AddSQLParam(@params, name, val, TypeConvertor.ToSqlDbType(val));
        }

        protected void AddSQLParam(SqlParameterCollection @params, string name, object val, SqlDbType type)
        {
            object paramValue = getParamValue(val);

            @params.Add(name, type).Value = paramValue;
        }
        private static object getParamValue(object val)
        {
            object paramValue = null;

            if (val is int || val is long || val is decimal || val is double || val is bool ||
                val is string || val is DateTime)
            {
                paramValue = val;
            }
            else if (val is DateTime?)
            {
                DateTime? dt;
                dt = (DateTime?)val;
                if (dt.HasValue)
                {
                    paramValue = dt.Value;
                }
                else
                {
                    paramValue = DBNull.Value;
                }
            }
            else if (val is decimal?)
            {
                decimal? dc;
                dc = (decimal?)val;
                if (dc.HasValue)
                {
                    paramValue = dc.Value;
                }
                else
                {
                    paramValue = DBNull.Value;
                }
            }
            else
            {
                paramValue = DBNull.Value;
            }
            return paramValue;
        }
        protected DataTable GetStoredProc(string sql, string[] condFields, object[] vals)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = CreateConnection())
            {
                conn.Open();
                dt = GetStoredProc(sql, condFields, vals, conn);
            }

            return dt;
        }
        protected DataTable GetStoredProc(string sql)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = CreateConnection())
            {
                conn.Open();
                dt = GetStoredProc(sql, conn);
                conn.Close();
            }

            return dt;
        }
        protected DataTable GetStoredProc(string sql, SqlConnection conn)
        {
            DataTable dt = new DataTable();

            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = conn.CreateCommand();
            adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            adapter.SelectCommand.CommandText = sql;
            adapter.SelectCommand.CommandTimeout = CommandTimeoutSecond;
            SqlParameterCollection param = adapter.SelectCommand.Parameters;
            param.Clear();
            adapter.Fill(dt);

            return dt;
        }
        protected DataTable GetStoredProc(string sql, string[] condFields, object[] vals, SqlConnection conn)
        {
            DataTable dt = new DataTable();

            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = conn.CreateCommand();
            adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            adapter.SelectCommand.CommandText = sql;
            adapter.SelectCommand.CommandTimeout = CommandTimeoutSecond;

            SqlParameterCollection param = adapter.SelectCommand.Parameters;
            param.Clear();
            for (int i = 0; i < condFields.Length; i++)
            {
                AddSQLParam(param, condFields[i], vals[i]);
            }
            adapter.Fill(dt);
            return dt;
        }

        #endregion
    }
}