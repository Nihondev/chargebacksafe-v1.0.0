﻿using ChargeBackEthoca.Models.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChargeBackEthoca.Models.Component
{
    public class Varible
    {
        public static EUser _user;
        public static EUser User
        {
            get
            {
                if (HttpRuntime.AppDomainAppId != null)
                {
                    //is web app
                    return (EUser)HttpContext.Current.Session["USER"];
                }
                else
                {
                    //is windows app
                    if (_user == null)
                    {
                        _user = new EUser
                        {
                            //member_id
                            USERID = Environment.UserName,
                            USERIP = Environment.MachineName
                        };
                    }
                    return _user;
                }
            }
            set
            {
                if (HttpRuntime.AppDomainAppId != null)
                {
                    HttpContext.Current.Session["USER"] = value;
                }
                else
                {
                    _user = value;
                }
            }
        }

    }
}