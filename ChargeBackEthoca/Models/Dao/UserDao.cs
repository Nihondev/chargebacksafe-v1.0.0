﻿using ChargeBackEthoca.Models.Component;
using ChargeBackEthoca.Models.Dto;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace ChargeBackEthoca.Models.Dao
{
    public class UserDao : BaseDao<UserDao>
    {
        private SqlConnection con;
        public string Regis(UserDto model, string action)
        {
            string result = "OK";
            try
            {
                con = CreateConnection();
                SqlCommand cmd = new SqlCommand("MSP01_USER_REGIS", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameterCollection param = cmd.Parameters;
                param.Clear();
                cmd.Parameters.AddWithValue("@ID", model.ID);
                cmd.Parameters.AddWithValue("@FIRST_NAME", model.FIRST_NAME);
                cmd.Parameters.AddWithValue("@LAST_NAME", model.LAST_NAME);
                cmd.Parameters.AddWithValue("@MEMBER_ID", 2);
                cmd.Parameters.AddWithValue("@USERNAME", model.USERNAME);
                cmd.Parameters.AddWithValue("@PASSWORD", DataCryptography.Encrypt(model.PASSWORD));
                cmd.Parameters.AddWithValue("@ADDRESS", model.ADDRESS);
                cmd.Parameters.AddWithValue("@PHONE", model.PHONE);
                cmd.Parameters.AddWithValue("@eMAIL", model.eMAIL);
                cmd.Parameters.AddWithValue("@CustomerId", model.CustomerId);
                cmd.Parameters.AddWithValue("@ACTION", action);
                con.Open();
                SqlDataReader read = cmd.ExecuteReader();
                while (read.Read())
                {
                    result = read.GetString(0).ToString();
                }
                con.Close();
            }
            catch (Exception e)
            {
                result = e.Message.ToString();
            }
            return result;
        }

        public bool ChkAuthDB(string vUser, ref int member_id, ref string pUName, ref string password, ref string CusId, ref string TimeZone)
        {
            try
            {
                DataTable dt = GetStoredProc("MSP02_ChkAuthDB", new string[] { "@USER_ID" }, new string[] { vUser });
                if (dt.Rows.Count > 0)
                {
                    member_id = Convert.ToInt32(dt.Rows[0]["MEMBER_ID"]);
                    pUName = dt.Rows[0]["USERNAME"].ToString();
                    password = dt.Rows[0]["PASSWORD"].ToString();
                    CusId = dt.Rows[0]["CustomerId"].ToString();
                    TimeZone = dt.Rows[0]["TimeZone"].ToString();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                //logger.Error(ex);
                throw ex;
            }
        }

        public string ChangePass(int id, string pass)
        {
            string result = "OK";
            try
            {
                con = CreateConnection();
                SqlCommand cmd = new SqlCommand("MSP04_CHANGE_PASSWORD", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameterCollection param = cmd.Parameters;
                param.Clear();
                AddSQLParam(param, "@ID", Convert.ToInt32(id));
                AddSQLParam(param, "@PASS", DataCryptography.Encrypt(pass.ToString().Trim()).ToString());
                con.Open();
                SqlDataReader read = cmd.ExecuteReader();
                while (read.Read())
                {
                    result = read.GetString(0).ToString();
                }
                con.Close();
            }
            catch (Exception e)
            {
                result = e.Message.ToString();
            }
            return result;
        }

        public string GetEMail(string Email)
        {
            string result = "OK";
            try
            {
                con = CreateConnection();
                SqlCommand cmd = new SqlCommand("MSP03_CHECK_USER", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameterCollection param = cmd.Parameters;
                param.Clear();
                AddSQLParam(param, "@Email", Email.ToString());
                con.Open();
                SqlDataReader read = cmd.ExecuteReader();
                if (read.Read())
                {
                    string username = read["USERNAME"].ToString();
                    string password = DataCryptography.Decrypt(read["PASSWORD"].ToString());

                    MailMessage rePass = new MailMessage("Nihondev@hotmail.com", Email.ToString());
                    rePass.From = new MailAddress("Nihondev@hotmail.com");
                    rePass.Subject = "Your Password";
                    rePass.Body = string.Format("Hello : <h1>{0}</h1> This is Your Email Username <br/> Your Password Is <h1>{1}</h1>", username, password);
                    rePass.IsBodyHtml = true;

                    SmtpClient smtp = new SmtpClient("smtp.live.com");
                    smtp.Port = 587;
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = new System.Net.NetworkCredential("Nihondev@hotmail.com", "vVuaZHc5DYg3xBP4fbs3UA==");
                    smtp.EnableSsl = true;
                    smtp.Send(rePass);
                }
                else
                {
                    result = "Username or email Is not exist";
                }
            }
            catch (Exception e)
            {
                result = e.Message.ToString();
            }
            return result;
        }
    }
}