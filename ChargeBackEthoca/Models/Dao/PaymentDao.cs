﻿using ChargeBackEthoca.Models.Component;
using ChargeBackEthoca.Models.Dto;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ChargeBackEthoca.Models.Dao
{
    public class PaymentDao : BaseDao<PaymentDao>
    {
        private SqlConnection con;
        private DataTable dt;
        public void LogReq(PaymentDto model, string action)
        {
            try
            {
                con = CreateConnection();
                SqlCommand cmd = new SqlCommand("MSP10_LOG_REQ", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameterCollection param = cmd.Parameters;
                param.Clear();
                AddSQLParam(param, "@ID", model.ID);
                AddSQLParam(param, "@Url", model.Url.ToString());
                AddSQLParam(param, "@objectBilling", model.objectBilling.ToString());
                AddSQLParam(param, "@create", model.create.ToString());
                AddSQLParam(param, "@customer", model.customer.ToString());
                AddSQLParam(param, "@liveMode", model.liveMode.ToString());
                AddSQLParam(param, "@returnUrl", model.returnUrl.ToString());
                AddSQLParam(param, "@ACTION", action);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<PaymentDto> ReqBilling()
        {
            try
            {
                List<PaymentDto> list = new List<PaymentDto>();
                dt = GetStoredProc("MSP11_BILLING_REC");
                foreach (DataRow dr in dt.Rows)
                {
                    list.Add(new PaymentDto
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        Url = dr["Url"].ToString(),
                        objectBilling = dr["objectBilling"].ToString(),
                        create = Convert.ToDateTime(dr["create_date"]),
                        customer = dr["customer"].ToString(),
                        liveMode = Convert.ToBoolean(dr["liveMode"]),
                        returnUrl = dr["returnUrl"].ToString(),
                    });
                }
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}