﻿using ChargeBackEthoca.Models.Component;
using ChargeBackEthoca.Models.Dto;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ChargeBackEthoca.Models.Dao
{
    public class SettingDao : BaseDao<SettingDao>
    {
        private SqlConnection con;
        public string TimeZone(SetiingDto model, string action)
        {
            string result = "OK";
            try
            {
                con = CreateConnection();
                SqlCommand cmd = new SqlCommand("MSP15_SETUP_TIMEZONE", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameterCollection param = cmd.Parameters;
                param.Clear();
                AddSQLParam(param, "@ID", Convert.ToInt32(model.ID));
                AddSQLParam(param, "@PASSWORD", DataCryptography.Encrypt(model.PASSWORD.ToString().Trim()));
                AddSQLParam(param, "@TimeZone", model.TimeZone.ToString());
                con.Open();
                SqlDataReader read = cmd.ExecuteReader();
                while (read.Read())
                {
                    result = read.GetString(0).ToString();
                }
                con.Close();
            }
            catch (Exception e)
            {
                result = e.Message.ToString();
            }
            return result;
        }


        public SelectList TimeZomeCollection()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            IReadOnlyCollection<TimeZoneInfo> timezone;
            timezone = TimeZoneInfo.GetSystemTimeZones();
            list.Add(new SelectListItem
            {
                Text = "Choose".ToString(),
                Value = "".ToString()
            });
            foreach (TimeZoneInfo time in timezone)
            {
                list.Add(new SelectListItem
                {
                    Text = time.DisplayName.ToString(),
                    Value = time.Id.ToString()
                });
            }
            return new SelectList(list, "Value", "Text");
        }

        public DateTime getCurrentDateTimeWithTimeZone(string strTimeZone)
        {
            var localTimezone = TimeZoneInfo.Local;
            var userTimezone = TimeZoneInfo.FindSystemTimeZoneById(strTimeZone);

            var todayDate = DateTime.Now;
            var todayLocal = new DateTimeOffset(todayDate,
                                                localTimezone.GetUtcOffset(todayDate));

            var todayUserOffset = TimeZoneInfo.ConvertTime(todayLocal, userTimezone);
            return todayUserOffset.DateTime;

        }
    }
}