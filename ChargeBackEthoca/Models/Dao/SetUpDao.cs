﻿using ChargeBackEthoca.Models.Component;
using ChargeBackEthoca.Models.Dto;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ChargeBackEthoca.Models.Dao
{
    public class SetUpDao : BaseDao<SetUpDao>
    {
        private SqlConnection con;
        private DataTable dt;
        public string KepTALiasMID(SetUpDto model , string UID, string action)
        {
            string result = "OK";
            try
            {
                con = CreateConnection();
                SqlCommand cmd = new SqlCommand("MSP16_SETUP_MIDAlias", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameterCollection param = cmd.Parameters;
                param.Clear();
                cmd.Parameters.AddWithValue("@ID", model.ID);
                cmd.Parameters.AddWithValue("@MID", model.MID);
                cmd.Parameters.AddWithValue("@MIDAlias", model.MIDAlias);
                cmd.Parameters.AddWithValue("@UID", UID);
                //cmd.Parameters.AddWithValue("@REMARK", model.REMARK);
                cmd.Parameters.AddWithValue("@ACTION", action);
                con.Open();
                SqlDataReader read = cmd.ExecuteReader();
                while (read.Read())
                {
                    result = read.GetString(0).ToString();
                }
                con.Close();
            }
            catch (Exception e)
            {
                result = e.Message.ToString();
            }
            return result;
        } 
        
        public string KepTDescriptor(SetUpDto model , string UID, string action)
        {
            string result = "OK";
            try
            {
                con = CreateConnection();
                SqlCommand cmd = new SqlCommand("MSP19_ESC_DESCRIPTOR", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameterCollection param = cmd.Parameters;
                param.Clear();
                cmd.Parameters.AddWithValue("@ID", model.ID);
                cmd.Parameters.AddWithValue("@MIDAlias", model.MIDAlias);
                cmd.Parameters.AddWithValue("@DESCRIPTOR", model.DESCRIPTOR);
                cmd.Parameters.AddWithValue("@UID", UID);
                cmd.Parameters.AddWithValue("@ACTION", action);
                con.Open();
                SqlDataReader read = cmd.ExecuteReader();
                while (read.Read())
                {
                    result = read.GetString(0).ToString();
                }
                con.Close();
            }
            catch (Exception e)
            {
                result = e.Message.ToString();
            }
            return result;
        }

        public string Del(int ID, string action)
        {
            string result = "OK";
            try
            {
                con = CreateConnection();
                SqlCommand cmd = new SqlCommand("MSP21_DEL_DESCRIPTOR", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameterCollection param = cmd.Parameters;
                param.Clear();
                cmd.Parameters.AddWithValue("@ID", ID);
                cmd.Parameters.AddWithValue("@ACTION", action);
                con.Open();
                SqlDataReader read = cmd.ExecuteReader();
                while (read.Read())
                {
                    result = read.GetString(0).ToString();
                }
                con.Close();
            }
            catch (Exception e)
            {
                result = e.Message.ToString();
            }
            return result;
        }
        public string Approve(int ID,string STATUS, string action)
        {
            string result = "OK";
            try
            {
                con = CreateConnection();
                SqlCommand cmd = new SqlCommand("MSP22_APPR_DESC", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameterCollection param = cmd.Parameters;
                param.Clear();
                AddSQLParam(param, "@ID", ID);
                AddSQLParam(param, "@STATUS", STATUS);
                AddSQLParam(param, "@ACTION", action);
                con.Open();
                SqlDataReader read = cmd.ExecuteReader();
                while (read.Read())
                {
                    result = read.GetString(0).ToString();
                }
                con.Close();
            }
            catch (Exception e)
            {
                result = e.Message.ToString();
            }
            return result;
        }
        public List<SetUpDto> MIDAlias(string UID, string USER_ROLE) 
        {
            try
            {
                List<SetUpDto> list = new List<SetUpDto>();
                dt = GetStoredProc("MSP17_MIDALIASES", new string[] { "@UID", "@USER_ROLE" }, new string[] { UID, USER_ROLE });
                foreach (DataRow dr in dt.Rows)
                {
                    list.Add(new SetUpDto
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        MID = dr["MID"].ToString(),
                        MIDAlias = dr["MIDAlias"].ToString(),
                        REMARK = dr["REMARK"].ToString(),
                        STATUS = Convert.ToInt32(dr["STATUS"]),
                    });
                }
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public Boolean ChkUserKey(string Key)
        {
            dt = GetStoredProc("MSP23_CHKMID", new string[] { "@Key" }, new string[] { Key });
            if (dt.Rows.Count > 0)
            {
                return false;
            }
            return true;
        }
        public List<SetUpDto> DEscriptor(string UID, string USER_ROLE)
        {
            try
            {
                List<SetUpDto> list = new List<SetUpDto>();
                dt = GetStoredProc("MSP20_GET_DESCRIPTOR", new string[] { "@UID", "@USER_ROLE" }, new string[] { UID, USER_ROLE });
                foreach (DataRow dr in dt.Rows)
                {
                    list.Add(new SetUpDto
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        DESCRIPTOR = dr["DESCRIPTOR"].ToString(),
                        MIDAlias = dr["MIDAlias"].ToString(),
                        REMARK = dr["REMARK"].ToString(),
                        CREATE_DATE = dr["CREATE_DATE"].ToString(),
                        STATUS = Convert.ToInt32(dr["STATUS"]),
                    });
                }
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public SelectList ALiasMIDCollection(string UID, string USER_ROLE)
        {

            List<SelectListItem> list = new List<SelectListItem>();
            dt = GetStoredProc("MSP18_LIST_ALIAS", new string[] { "@UID", "@USER_ROLE" }, new string[] { UID, USER_ROLE });
            list.Add(new SelectListItem
            {
                Text = "Choose".ToString(),
                Value = "".ToString()
            });
            foreach (DataRow dr in dt.Rows)
            {
                list.Add(new SelectListItem
                {
                    Text = dr["MIDAlias"].ToString(),
                    Value = dr["MID"].ToString()
                });
            }
            return new SelectList(list, "Value", "Text");
        }

        public SelectList GetStatusApprove()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            list.Add(new SelectListItem { Text = "Choose", Value = "" });
            list.Add(new SelectListItem { Text = "No", Value = "2" });
            list.Add(new SelectListItem { Text = "Yes", Value = "1" });
            return new SelectList(list, "Value", "Text");
        }
        public SelectList GetStatusRespond()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            list.Add(new SelectListItem { Text = "Choose", Value = "" });
            list.Add(new SelectListItem { Text = "I ISSUED A FULL REFUND", Value = "00080" });
            list.Add(new SelectListItem { Text = "THE ORDER WAS ALREADY REFUNDED, SO THERE IS NOTHING FOR ME TO DO", Value = "00060" });
            list.Add(new SelectListItem { Text = "I WANT TO FIGHT THIS CHARGEBACK (DO NOTHING)", Value = "00030" });
            list.Add(new SelectListItem { Text = "I UNABLE TO FIND THIS ORDER", Value = "00040" });
            return new SelectList(list, "Value", "Text");
        }
    }
}