﻿using ChargeBackEthoca.Models.Component;
using ChargeBackEthoca.Models.Dto;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ChargeBackEthoca.Models.Dao
{
    public class EthocaDao : BaseDao<EthocaDao>
    {
        private SqlConnection con;
        private DataTable dt;
        public string InsDataResponse(EthocaDto model, string action)
        {
            string result = string.Empty;
            try
            {
                con = CreateConnection();
                SqlCommand cmd = new SqlCommand("MSP05_LOG_REG", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameterCollection param = cmd.Parameters;
                param.Clear();
                AddSQLParam(param, "@ETHOCA_ID", model.ethoca_id.ToString());
                AddSQLParam(param, "@TYPE", model.type.ToString());
                AddSQLParam(param, "@ALERT", model.alert_timestamp.ToString());
                AddSQLParam(param, "@AGE", model.age.ToString());
                AddSQLParam(param, "@ISSUER", model.issuer.ToString());
                AddSQLParam(param, "@CARD", model.card_number.ToString());
                AddSQLParam(param, "@CARD_BIN", model.card_bin.ToString());
                AddSQLParam(param, "@CARD_LAST4", model.card_last4.ToString());
                AddSQLParam(param, "@ARN", model.arn.ToString());
                AddSQLParam(param, "@TIMESTAMP", model.transaction_timestamp.ToString());
                AddSQLParam(param, "@MERCHANT", model.merchant_descriptor.ToString());
                AddSQLParam(param, "@MEMBER_ID", Convert.ToInt32(model.member_id));
                AddSQLParam(param, "@MCC", model.mcc.ToString());
                AddSQLParam(param, "@AMOUNT", Convert.ToDecimal(model.amount));
                AddSQLParam(param, "@CURRENCY", model.currency.ToString());
                AddSQLParam(param, "@TRANSACTION_TYPE", model.transaction_type.ToString());
                AddSQLParam(param, "@INITTIAL", model.initiated_by.ToString());
                AddSQLParam(param, "@IS_3D", model.is_3d_secure.ToString());
                AddSQLParam(param, "@SOUCRE", model.source.ToString());
                AddSQLParam(param, "@AUTH", model.auth_code.ToString());
                AddSQLParam(param, "@MERCHANT_MEMBER_NAME", model.merchant_member_name.ToString());
                AddSQLParam(param, "@TRANSACTION_ID", model.transaction_id.ToString());
                AddSQLParam(param, "@REASON_CODE", model.chargeback_reason_code.ToString());
                AddSQLParam(param, "@CHARGE_BACK", model.chargeback_amount.ToString());
                AddSQLParam(param, "@CHARGE_BACK_CURRENCY", model.chargeback_currency.ToString());
                AddSQLParam(param, "@ACTION", action);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                result = "OK";
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }


        public string LogResponse(int ID, string STATUS, string action)
        {
            string result = "OK";
            try
            {
                con = CreateConnection();
                SqlCommand cmd = new SqlCommand("MSP28_RESPONDSE_API", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameterCollection param = cmd.Parameters;
                param.Clear();
                AddSQLParam(param, "@ID", Convert.ToInt32(ID));
                AddSQLParam(param, "@STATUS", STATUS.ToString());
                AddSQLParam(param, "@ACTION", action);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
        public List<EthocaDto> Respond(string UID, string ROLE)
        {
            try
            {
                List<EthocaDto> list = new List<EthocaDto>();
                dt = GetStoredProc("MSP06_RESPOND",new string[] { "@UID", "@ROLE"}, new string[] { UID, ROLE });
                foreach (DataRow dr in dt.Rows)
                {
                    list.Add(new EthocaDto
                    {
                        ROW_ID = Convert.ToInt32(dr["ROW_ID"]),
                        ethoca_id = dr["ETHOCA_ID"].ToString(),
                        RESPOND = dr["RESPOND"].ToString(),
                        type = dr["TYPE"].ToString(),
                        TIME_RECIPE = dr["TIME_RECIPE"].ToString(),
                        EXD = dr["EXD"].ToString().Replace("-", ""),
                        alert_timestamp = dr["ALERT_TIMESTAMP"].ToString(),
                        age = dr["AGE"].ToString(),
                        issuer = dr["ISSUER"].ToString(),
                        card_number = dr["CARD_NUMBER"].ToString(),
                        card_bin = dr["CARD_BIN"].ToString(),
                        card_last4 = dr["CARD_LAST4"].ToString(),
                        transaction_timestamp = dr["TRANSACTION_TIMESTAMP"].ToString(),
                        merchant_descriptor = dr["MERCHANT_DESCRIPTION"].ToString(),
                        member_id = Convert.ToInt32(dr["MEMBER_ID"]),
                        mcc = dr["MCC"].ToString(),
                        amount = Convert.ToDecimal(dr["AMOUNT"] + ".00"),
                        currency = dr["CURRENCY"].ToString(),
                        transaction_type = dr["TRANSACTION_TYPE"].ToString(),
                        initiated_by = dr["INITIATED_BY"].ToString(),
                        is_3d_secure = dr["IS_3D_SECURE"].ToString(),
                        source = dr["SOURCE"].ToString(),
                        auth_code = dr["AUTH_CODE"].ToString(),
                        merchant_member_name = dr["MERCHANT"].ToString(),
                        transaction_id = dr["TRANSACTION_ID"].ToString(),
                        chargeback_reason_code = dr["CHARGEBACK_REASON_CODE"].ToString(),
                        chargeback_amount = Convert.ToDecimal(dr["CHARGEBACK_AMOUNT_CODE"]),
                        chargeback_currency = dr["CHARGEBACK_CURRENCY"].ToString(),
                    });
                }
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<EthocaDto> Archived(string UID, string ROLE) 
        {
            try
            {
                List<EthocaDto> list = new List<EthocaDto>();
                dt = GetStoredProc("MSP13_ARCHIVED", new string[] { "@UID", "@ROLE" }, new string[] { UID, ROLE });
                foreach (DataRow dr in dt.Rows)
                {
                    list.Add(new EthocaDto
                    {
                        ROW_ID = Convert.ToInt32(dr["ROW_ID"]),
                        ethoca_id = dr["ETHOCA_ID"].ToString(),
                        RESPOND = dr["RESPOND"].ToString(),
                        type = dr["TYPE"].ToString(),
                        EXD = dr["EXD"].ToString().Replace("-", ""),
                        TIME_RECIPE = dr["TIME_RECIPE"].ToString(),
                        alert_timestamp = dr["ALERT_TIMESTAMP"].ToString(),
                        age = dr["AGE"].ToString(),
                        issuer = dr["ISSUER"].ToString(),
                        card_number = dr["CARD_NUMBER"].ToString(),
                        card_bin = dr["CARD_BIN"].ToString(),
                        card_last4 = dr["CARD_LAST4"].ToString(),
                        transaction_timestamp = dr["TRANSACTION_TIMESTAMP"].ToString(),
                        merchant_descriptor = dr["MERCHANT_DESCRIPTION"].ToString(),
                        member_id = Convert.ToInt32(dr["MEMBER_ID"]),
                        mcc = dr["MCC"].ToString(),
                        amount = Convert.ToDecimal(dr["AMOUNT"] + ".00"),
                        currency = dr["CURRENCY"].ToString(),
                        transaction_type = dr["TRANSACTION_TYPE"].ToString(),
                        initiated_by = dr["INITIATED_BY"].ToString(),
                        is_3d_secure = dr["IS_3D_SECURE"].ToString(),
                        source = dr["SOURCE"].ToString(),
                        auth_code = dr["AUTH_CODE"].ToString(),
                        merchant_member_name = dr["MERCHANT"].ToString(),
                        transaction_id = dr["TRANSACTION_ID"].ToString(),
                        chargeback_reason_code = dr["CHARGEBACK_REASON_CODE"].ToString(),
                        chargeback_amount = Convert.ToDecimal(dr["CHARGEBACK_AMOUNT_CODE"]),
                        chargeback_currency = dr["CHARGEBACK_CURRENCY"].ToString(),
                    });
                }
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}