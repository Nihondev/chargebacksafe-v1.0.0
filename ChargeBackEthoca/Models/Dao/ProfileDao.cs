﻿using ChargeBackEthoca.Models.Component;
using ChargeBackEthoca.Models.Dto;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace ChargeBackEthoca.Models.Dao
{
    public class ProfileDao : BaseDao<ProfileDao>
    {
        private DataTable dt;
        public List<ProfileDto> GetDataUser() //int UID
        {
            try
            {
                List<ProfileDto> list = new List<ProfileDto>();
                dt = GetStoredProc("MSP07_USER_INFO"); //, new string[] { "@UID" }, new string[] { UID }
                foreach (DataRow dr in dt.Rows)
                {
                    list.Add(new ProfileDto
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        FIRST_NAME = dr["FIRST_NAME"].ToString(),
                        LAST_NAME = dr["LAST_NAME"].ToString(),
                        USERNAME = dr["USERNAME"].ToString(),
                        PASSWORD = dr["PASSWORD"].ToString(),
                        eMAIL = dr["EMAIL"].ToString(),
                        PHONE = dr["PHONE"].ToString(),
                        ADDRESS = dr["ADDRESS"].ToString(),
                        CustomerId = dr["CustomerId"].ToString(),
                    });
                }
                return list;
            }
            catch (Exception EX)
            {
                throw EX;
            }
        }

        public List<ProfileDto> GetDataUserById(string UID) //int UID
        {
            try
            {
                List<ProfileDto> list = new List<ProfileDto>();
                dt = GetStoredProc("MSP12_USER_LDAB", new string[] { "@UID" }, new string[] { UID }); //
                foreach (DataRow dr in dt.Rows)
                {
                    list.Add(new ProfileDto
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        FIRST_NAME = dr["FIRST_NAME"].ToString(),
                        LAST_NAME = dr["LAST_NAME"].ToString(),
                        USERNAME = dr["USERNAME"].ToString(),
                        PASSWORD = dr["PASSWORD"].ToString(),
                        eMAIL = dr["EMAIL"].ToString(),
                        PHONE = dr["PHONE"].ToString(),
                        ADDRESS = dr["ADDRESS"].ToString(),
                        CustomerId = dr["CustomerId"].ToString(),
                    });
                }
                return list;
            }
            catch (Exception EX)
            {
                throw EX;
            }
        }
    }
}