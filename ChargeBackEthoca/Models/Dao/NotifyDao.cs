﻿using ChargeBackEthoca.Models.Component;
using ChargeBackEthoca.Models.Dto;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace ChargeBackEthoca.Models.Dao
{
    public class NotifyDao : BaseDao<NotifyDao>
    {
        private SqlConnection con;
        private DataTable dt;

        public List<NotifyDto> Notify(string UID, string USER_ROLE)
        {
            try
            {
                List<NotifyDto> list = new List<NotifyDto>();
                dt = GetStoredProc("MSP25_EMAIL", new string[] { "@UID", "@USER_ROLE" }, new string[] { UID, USER_ROLE });
                foreach (DataRow dr in dt.Rows)
                {
                    list.Add(new NotifyDto
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        MIDAlias = dr["MIDAlias"].ToString(),
                        EMAIL_ALERT = dr["EMAIL_ALERT"].ToString(),
                        EMAIL_REQ_TYPE = Convert.ToInt32(dr["EMAIL_REQ_TYPE"]),
                        CREATE_DATE = dr["CREATE_DATE"].ToString(),
                        STATUS = Convert.ToInt32(dr["STATUS"]),
                    });
                }
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string EmailS(NotifyDto model, string UID, string action)
        {
            string result = "OK";
            try
            {
                con = CreateConnection();
                SqlCommand cmd = new SqlCommand("MSP24_EMAIL_REC", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameterCollection param = cmd.Parameters;
                param.Clear();
                cmd.Parameters.AddWithValue("@ID", model.ID);
                cmd.Parameters.AddWithValue("@MIDAlias", model.MIDAlias);
                cmd.Parameters.AddWithValue("@EMAIL_ALERT", model.EMAIL_ALERT);
                cmd.Parameters.AddWithValue("@UID", UID);
                cmd.Parameters.AddWithValue("@ACTION", action);
                con.Open();
                SqlDataReader read = cmd.ExecuteReader();
                while (read.Read())
                {
                    result = read.GetString(0).ToString();
                }
                con.Close();
            }
            catch (Exception e)
            {
                result = e.Message.ToString();
            }
            return result;
        }

        public string EmailDaily(int ID, string STATUS, string action)
        {
            string result = "OK";
            try
            {
                con = CreateConnection();
                SqlCommand cmd = new SqlCommand("MSP26_EMAIL_UPD", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameterCollection param = cmd.Parameters;
                param.Clear();
                AddSQLParam(param, "@ID", ID);
                AddSQLParam(param, "@STATUS", STATUS);
                AddSQLParam(param, "@ACTION", action);
                con.Open();
                SqlDataReader read = cmd.ExecuteReader();
                while (read.Read())
                {
                    result = read.GetString(0).ToString();
                }
                con.Close();
            }
            catch (Exception e)
            {
                result = e.Message.ToString();
            }
            return result;
        }

        public string Send() //string Address, int alert
        {
            string result = "OK";
            string mPass = "vVuaZHc5DYg3xBP4fbs3UA==";
            string mUse = "Nihondev@hotmail.com";

            dt = GetStoredProc("MSP27_SEND_ALERT");
            foreach (DataRow dr in dt.Rows)
            {
                string SUMMARY = dr["MIDAlias"].ToString();
                string EMAIL = dr["EMAIL_ALERT"].ToString();

                using (MailMessage mm = new MailMessage(mUse, EMAIL))
                {
                    mm.Subject = "Chargebaksafe Alerts Daily";
                    mm.Body = string.Format("<b>You have alert </b>{0}<br /><br />chargeback of the day.", SUMMARY);

                    mm.IsBodyHtml = true;
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = "smtp.gmail.com";
                    smtp.EnableSsl = true;
                    System.Net.NetworkCredential credentials = new System.Net.NetworkCredential();
                    credentials.UserName = mUse;
                    credentials.Password = mPass;
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = credentials;
                    smtp.Port = 587;
                    smtp.Send(mm);
                }
            }


            //SmtpClient SmtpServer = new SmtpClient("smtp.live.com");
            //try
            //{
            //    var mail = new MailMessage();
            //    mail.From = new MailAddress(mUse);
            //    mail.Subject = "Chargebacksafe Alert Daily";
            //    mail.SubjectEncoding = System.Text.Encoding.UTF8;
            //    mail.IsBodyHtml = true;
            //    mail.BodyEncoding = System.Text.Encoding.UTF8;
            //    mail.To.Add(new MailAddress(Address));
            //    string htmlBody;
            //    htmlBody = "You Have" + alert + "Alert Today" + "https://www.google.com";
            //    mail.Body = htmlBody;
            //    SmtpServer.Port = 587;
            //    SmtpServer.UseDefaultCredentials = false;
            //    SmtpServer.Credentials = new System.Net.NetworkCredential(mUse, mPass);
            //    SmtpServer.EnableSsl = true;
            //    SmtpServer.Send(mail);
            //}
            //catch (Exception e)
            //{
            //    result = e.Message.ToString();
            //}
            return result;
        }


        public List<NotifyDto> ToEmail()
        {
            try
            {
                List<NotifyDto> list = new List<NotifyDto>();
                dt = GetStoredProc("MSP25_EMAIL");
                foreach (DataRow dr in dt.Rows)
                {
                    list.Add(new NotifyDto
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        MIDAlias = dr["MIDAlias"].ToString(),
                        EMAIL_ALERT = dr["EMAIL_ALERT"].ToString(),
                        EMAIL_REQ_TYPE = Convert.ToInt32(dr["EMAIL_REQ_TYPE"]),
                        CREATE_DATE = dr["CREATE_DATE"].ToString(),
                        STATUS = Convert.ToInt32(dr["STATUS"]),
                    });
                }
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int AlertDaily()
        {
            int clientMail = 0;
            dt = GetStoredProc("PR070_TO_EMAIL");
            foreach (DataRow row in dt.Rows)
            {
                clientMail = Convert.ToInt32(row["clientMail"]);
            }
            return clientMail;
        }
    }
}