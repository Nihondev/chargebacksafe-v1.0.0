﻿using ChargeBackEthoca.Models.Component;
using ChargeBackEthoca.Models.Dto;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ChargeBackEthoca.Models.Dao
{
    public class LogDao : BaseDao<LogDao>
    {
        private SqlConnection con;
        private DataTable dt;
        public void LogUsed(LogDto model, string action)
        {
            try
            {
                con = CreateConnection();
                SqlCommand cmd = new SqlCommand("MSP08_LOG_USED", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameterCollection param = cmd.Parameters;
                param.Clear();
                AddSQLParam(param, "@IP", model.IP.ToString());
                AddSQLParam(param, "@URL", model.URL.ToString());
                AddSQLParam(param, "@BROWSER", model.BROWSER.ToString());
                AddSQLParam(param, "@SYSTEM_OS", model.SYSTEM_OS.ToString());
                AddSQLParam(param, "@CONNECTION_HARDWARE", model.CONNECTION_HARDWARE.ToString());
                AddSQLParam(param, "@ACTION", action);

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {
                //logger.Error(ex);
                throw ex;
            }
        }

        public List<LogDto> Log()
        {
            try
            {
                List<LogDto> list = new List<LogDto>();
                dt = GetStoredProc("MSP09_GET_LOG");
                foreach (DataRow dr in dt.Rows)
                {
                    list.Add(
                    new LogDto
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        IP = dr["IP"].ToString(),
                        SYSTEM_OS = dr["SYSTEM_OS"].ToString(),
                        URL = dr["URL"].ToString(),
                        BROWSER = dr["BROWSER"].ToString(),
                        CONNECTION_HARDWARE = dr["CONNECTION_HARDWARE"].ToString(),
                        DATE = dr["DATE"].ToString(),
                    });
                }

                return list;
            }
            catch (Exception ex)
            {
                //logger.Error(ex);
                throw ex;
            }
        }
    }
}