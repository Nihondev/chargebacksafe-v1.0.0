﻿using ChargeBackEthoca.Models.Component;
using ChargeBackEthoca.Models.Dto;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace ChargeBackEthoca.Models.Dao
{
    public class ToEmail : BaseDao<ToEmail>
    {
        private DataTable dt;
        public string To()
        {
            string clientMail = "";
            dt = GetStoredProc("");
            foreach (DataRow row in dt.Rows)
            {
                clientMail = row["clientMail"].ToString();
            }
            return clientMail;
        }

        public List<EmailDto> GetSendDesc()
        {
            try
            {
                List<EmailDto> list = new List<EmailDto>();
                dt = GetStoredProc("MSP27_SEND_ALERT");
                foreach (DataRow dr in dt.Rows)
                {
                    list.Add(
                        new EmailDto
                        {
                            EMAIL_ALERT = dr["EMAIL_ALERT"].ToString(),
                            SUMMARY = dr["SUMMARY"].ToString(),
                        });
                }

                return list;
            }
            catch (Exception ex)
            {
                //logger.Error(ex);
                throw ex;
            }
        }
    }
}