﻿$(document).ready(function () {
    $("#tab2").click(function () {
        $("#dis1").attr("hidden", "hidden");
        $("#dis2").removeAttr("hidden");
    })

    $("#tab1").click(function () {
        $("#dis1").removeAttr("hidden", "hidden");
        $("#dis2").attr("hidden", "hidden");
    })

    $("#merchant_id").change(function () {
        $("#display").removeAttr("hidden");
    });
});

function updatedata(ID) {
    var ID = ID;
    var STATUS = $("#STAT").val();
    $.ajax({
        type: 'GET',
        url: '/Setup/Updatedata',
        data: { ID: ID, STATUS: STATUS },
        contentType: 'application/json; charset=UTF-8',
        success: function (response) {
        },
        error: function () {
            alert("Successfully");
            location.reload()
        }
    });
}

$("#sort_mid").change(function () {
    auto_qr(this.value)
});

function auto_qr(ID) {
    $.ajax({
        type: "POST",
        url: "/Setup/auto_query",
        data: ID,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            alert(msg);
            location.reload();
        }
    });
}
