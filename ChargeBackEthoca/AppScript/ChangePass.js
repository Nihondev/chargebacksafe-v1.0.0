﻿function ChangePass() {
    var ID = $("#ID").val();
    var TimeZone = $("#time_zone").val();
    var old_pass = $("#oldsPass").val();
    console.log("OldBack:" + old_pass )
    var chk_old_pass = $("#txtPassOld").val();
    console.log("OldKey:" + chk_old_pass)
    var new_pass = $("#txtPassNew").val();
    var chk_new_pass = $("#txtChkPassNew").val();
    if (old_pass != chk_old_pass) {
        alert("Password Invalid !");
        return;
    }
    if (new_pass != chk_new_pass) {
        alert("New Password Invalid !");
        return;
    }

    var SettingDto = {}
    SettingDto.ID = ID
    SettingDto.TimeZone = TimeZone
    SettingDto.PASSWORD = new_pass
    $.ajax({
        type: 'POST',
        url: '/Setting/SetUpTimeZone',
        data: JSON.stringify(SettingDto),
        contentType: 'application/json; charset=UTF-8',
        success: function (response) {
            alert("Successfully");
            cleartextboxes();
        },
        error: function () {
            alert("Error please try again later!!!");
            cleartextboxes();
        }
    });
}

function cleartextboxes() {
    $("input:password").val("");
}