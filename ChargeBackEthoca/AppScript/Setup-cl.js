﻿$(document).ready(function () {
    $("#tab2").click(function () {
        $("#dis1").attr("hidden", "hidden");
        $("#dis2").removeAttr("hidden");
    })

    $("#tab1").click(function () {
        $("#dis1").removeAttr("hidden", "hidden");
        $("#dis2").attr("hidden", "hidden");
    })

    $("input[type=text]#key").on("keyup", function () {
        $("#chk").removeAttr("disabled");
    });
});

function checkMid() {
    var Key = $('#key').val();
    $.ajax({
        type: 'POST',
        url: '/Setup/ChkMid/',
        data: {
            Key: Key,
        },
        success: function (response) {
            console.log(response)
            if (response == 'True') {
                alert("Successfully");
                $("#bedone").removeAttr("disabled", "disabled");
            }
            else {
                alert("MID has already exist !!!");
            }
        }
    });
}