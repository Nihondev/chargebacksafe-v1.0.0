﻿using ChargeBackEthoca.Models.Component;
using ChargeBackEthoca.Models.Dao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ChargeBackEthoca.Controllers.Member
{
    public class MemberController : Controller
    {
        Permisstion chkUser = new Permisstion();
        // GET: Member
        public ActionResult Index()
        {
            chkUser.chkrights("USER");
            Session["USERNAME"] = Varible.User.USERNAME;
            Session["CustomerId"] = Varible.User.CustomerId;
            Session["ROLE"] = Varible.User.MEMBER_ID;
            var ds = ProfileDao.Instance.GetDataUser();
            return View(ds);
        }
    }
}