﻿using ChargeBackEthoca.Models.Component;
using ChargeBackEthoca.Models.Dao;
using ChargeBackEthoca.Repository;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ChargeBackEthoca.Controllers.Stripe
{
    public class StripeController : Controller
    {
        Permisstion chkUser = new Permisstion();
        // GET: Stripe
        public ActionResult Index(int? page)
        {
            chkUser.chkrights("USER");
            Session["USERNAME"] = Varible.User.USERNAME;
            Session["CustomerId"] = Varible.User.CustomerId;
            Session["ROLE"] = Varible.User.MEMBER_ID;
            var Data = PaymentDao.Instance.ReqBilling();
            var count = Data.Count.ToString();
            TempData["data"] = Data.ToList().ToPagedList(page ?? 1, 10);
            Session["Count"] = count;
            ViewBag.Count = Session["Count"];
            return View(TempData["data"]);
        }
        public ActionResult Billing()
        {
            chkUser.chkrights("USER");
            string UID = Varible.User.USERNAME;
            Session["USERNAME"] = Varible.User.USERNAME;
            Session["CustomerId"] = Varible.User.CustomerId;
            Session["ROLE"] = Varible.User.MEMBER_ID;
            var DataUser = ProfileDao.Instance.GetDataUserById(UID);
            return View(DataUser);
        }
    }
}