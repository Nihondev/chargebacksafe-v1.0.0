﻿using ChargeBackEthoca.Models.Component;
using ChargeBackEthoca.Models.Dao;
using ChargeBackEthoca.Models.Dto;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace ChargeBackEthoca.Controllers.Notification
{
    public class NotificationController : Controller
    {
        Permisstion chkUser = new Permisstion();
        // GET: Notification
        public ActionResult Index()
        {
            chkUser.chkrights("USER");
            string UID = Varible.User.USERNAME;
            string USER_ROLE = Varible.User.MEMBER_ID.ToString();
            ViewBag.Status = SetUpDao.Instance.GetStatusApprove();
            ViewBag.MIDALiasList = SetUpDao.Instance.ALiasMIDCollection(UID, USER_ROLE);
            ViewBag.DS_EMAIL = NotifyDao.Instance.Notify(UID, USER_ROLE);
            return View();
        }
        [HttpPost]
        public ActionResult Index(NotifyDto model)
        {
            try
            {
                string UID = Varible.User.USERNAME;
                string result = NotifyDao.Instance.EmailS(model, UID, "INS");
                if (result != "OK")
                {
                    ModelState.Clear();
                    ViewBag.Message = "Error";
                    TempData["Message"] = ViewBag.Message;
                }
                else
                {
                    ViewBag.Message = "Successfully";
                    TempData["Message"] = ViewBag.Message;
                }
            }
            catch
            {
                return RedirectToAction("Index", "Error");
            }
            return RedirectToAction("Index", "Notification");
        }

        public ActionResult Updatedata(int ID, string STATUS)
        {
            try
            {
                string result = NotifyDao.Instance.EmailDaily(ID, STATUS, "UPD");
                if (result != "OK")
                {
                    return Json(result);
                }
                else
                {
                    return Json("Successfully !");
                }

            }
            catch
            {
                return Json("Error !!");
            }
        }
        public void SendEmailDaily()
        {
            var mail = NotifyDao.Instance.ToEmail();
            int alert = NotifyDao.Instance.AlertDaily();
            string Address = mail[0].EMAIL_ALERT;
            var data = NotifyDao.Instance.Send();
        }

        public ActionResult SettingAdmin()
        {
            return View();
        }
    }
}