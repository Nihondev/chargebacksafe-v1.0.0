﻿using ChargeBackEthoca.Models.Component;
using ChargeBackEthoca.Models.Dao;
using ChargeBackEthoca.Models.Dto;
using ChargeBackEthoca.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ChargeBackEthoca.Controllers.Transaction
{
    public class TransactionController : Controller
    {
        Permisstion chkUser = new Permisstion();
        // GET: Transaction
        public ActionResult Index()
        {
            chkUser.chkrights("USER");
            Session["USERNAME"] = Varible.User.USERNAME;
            Session["CustomerId"] = Varible.User.CustomerId;
            Session["ROLE"] = Varible.User.MEMBER_ID;

            string UID = Varible.User.USERNAME;
            string ROLE = Varible.User.MEMBER_ID.ToString();
            SettingDao app = new SettingDao();
            string LocalTime = Varible.User.TimeZone.ToString();
            if (LocalTime != "")
            {
                ViewBag.TimeZone = app.getCurrentDateTimeWithTimeZone(LocalTime);
                Session["TimeZone"] = ViewBag.TimeZone;
            }
            if (Session["USERNAME"] != null)
            {
                EUser user = new EUser();
                user.USERIP = GetUserIP();
                var Da = Request.Browser.IsMobileDevice;
                LogDto log = new LogDto();
                log.IP = user.USERIP;
                log.URL = Request.Url.AbsoluteUri;
                log.BROWSER = GetWebBrowserName();
                log.SYSTEM_OS = Info.GetHardware();
                log.CONNECTION_HARDWARE = Request.Browser.IsMobileDevice == true ? "Mobile" : "Computer";
                LogDao.Instance.LogUsed(log, "log");
            }
            ViewBag.Status = SetUpDao.Instance.GetStatusRespond();
            ViewBag.Record = EthocaDao.Instance.Respond(UID, ROLE); //ethodca_id, issuer, merchant, type, sdate, ndate
            ViewBag.Archived = EthocaDao.Instance.Archived(UID, ROLE); //ethodca_id, issuer, merchant, type, sdate, ndate
            return View();
        }

        [HttpPost]
        public JsonResult Response(int ID, string STATUS)
        {
            string result = EthocaDao.Instance.LogResponse(ID, STATUS, "UPD");
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        private string GetUserIP()
        {
            string ipList = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipList))
            {
                return ipList.Split(',')[0];
            }

            return Request.ServerVariables["REMOTE_ADDR"];
        }
        public string GetWebBrowserName()
        {
            string WebBrowserName = string.Empty;
            try
            {
                WebBrowserName = Request.Browser.Browser;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return WebBrowserName;
        }
    }
}