﻿using ChargeBackEthoca.Models.Component;
using ChargeBackEthoca.Models.Dao;
using ChargeBackEthoca.Models.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ChargeBackEthoca.Controllers.Setup
{
    public class SetupController : Controller
    {
        // GET: Setup
        Permisstion chkUser = new Permisstion();
        public ActionResult Index()
        {
            chkUser.chkrights("USER");
            string UID = Varible.User.USERNAME;
            string USER_ROLE = Varible.User.MEMBER_ID.ToString();
            ViewBag.MidAlias = SetUpDao.Instance.MIDAlias(UID, USER_ROLE);
            ViewBag.Descriptor = SetUpDao.Instance.DEscriptor(UID, USER_ROLE);
            ViewBag.MIDALiasList = SetUpDao.Instance.ALiasMIDCollection(UID, USER_ROLE);
            return View();
        }
        [HttpPost]
        public ActionResult Index(SetUpDto model)
        {
            try
            {
                string UID = Varible.User.USERNAME;
                string result = SetUpDao.Instance.KepTALiasMID(model, UID, "INS");
                if (result != "OK")
                {
                    ModelState.Clear();
                    ViewBag.Message = "Error";
                    TempData["Message"] = ViewBag.Message;
                }
                else
                {
                    ViewBag.Message = "Successfully";
                    TempData["Message"] = ViewBag.Message;
                }
            }
            catch
            {
                return RedirectToAction("Index", "Error");
            }
            return RedirectToAction("Index", "Setup");
        }
        public ActionResult Appr()
        {
            chkUser.chkrights("USER");
            string UID = Varible.User.USERNAME;
            string USER_ROLE = Varible.User.MEMBER_ID.ToString();
            ViewBag.MidAlias = SetUpDao.Instance.MIDAlias(UID, USER_ROLE);
            ViewBag.Descriptor = SetUpDao.Instance.DEscriptor(UID, USER_ROLE);
            ViewBag.MIDALiasList = SetUpDao.Instance.ALiasMIDCollection(UID, USER_ROLE);
            return View();
        }

        public ActionResult Updatedata(int ID, string STATUS)
        {
            try
            {
                string result = SetUpDao.Instance.Approve(ID, STATUS, "APPR");
                if (result != "OK")
                {
                    return Json(result);
                }
                else
                {
                    return Json("Successfully !");
                }

            }
            catch
            {
                return Json("Error !!");
            }
        }

        [HttpPost]
        public ActionResult Desciptor(SetUpDto model)
        {
            try
            {
                string UID = Varible.User.USERNAME;
                string result = SetUpDao.Instance.KepTDescriptor(model, UID, "INS");
                if (result != "OK")
                {
                    ModelState.Clear();
                    ViewBag.Message = "Error";
                    TempData["Message"] = ViewBag.Message;
                }
                else
                {
                    ViewBag.Message = "Successfully";
                    TempData["Message"] = ViewBag.Message;
                }
            }
            catch
            {
                return RedirectToAction("Index", "Error");
            }
            return RedirectToAction("Index", "Setup");
        }
        public bool ChkMid(string Key)
        {
            var Chk = SetUpDao.Instance.ChkUserKey(Key);
            return Chk;
        }

        public ActionResult Edit(int ID)
        {
            chkUser.chkrights("USER");
            string UID = Varible.User.USERNAME;
            string USER_ROLE = Varible.User.MEMBER_ID.ToString();
            ViewBag.MIDALiasList = SetUpDao.Instance.ALiasMIDCollection(UID, USER_ROLE);
            var model = SetUpDao.Instance.DEscriptor(UID, USER_ROLE).Find(smodel => smodel.ID == ID);
            return View(model);
        }
        [HttpPost]
        public ActionResult Edit(SetUpDto model)
        {
            try
            {
                string UID = Varible.User.USERNAME;
                string result = SetUpDao.Instance.KepTDescriptor(model, UID, "UPD");
                if (result != "OK")
                {
                    ViewBag.Message = "Error";
                    TempData["Message"] = ViewBag.Message;
                    ModelState.Clear();
                }
                else
                {
                    ViewBag.Message = "Successfully";
                    TempData["Message"] = ViewBag.Message;
                    return RedirectToAction("Index", "Setup");
                }
                return View();
            }
            catch
            {
                return RedirectToAction("Index", "Error");
            }
        }

        public ActionResult Delete(int ID)
        {
            try
            {
                string result = SetUpDao.Instance.Del(ID, "DEL");
                if (result != "OK")
                {
                    ViewBag.Message = "Error";
                    TempData["Message"] = ViewBag.Message;
                }
                else
                {
                    ViewBag.Message = "Deleleted";
                    TempData["Message"] = ViewBag.Message;
                    return RedirectToAction("Index", "Setup");
                }
            }
            catch
            {
                return RedirectToAction("Index", "Error");
            }
            return View();
        }

    }
}