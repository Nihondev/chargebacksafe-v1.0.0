﻿using ChargeBackEthoca.Models.Component;
using ChargeBackEthoca.Models.Dao;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ChargeBackEthoca.Controllers.Log
{
    public class LogController : Controller
    {
        Permisstion chkUser = new Permisstion();
        // GET: Log
        public ActionResult Index(int? page, string keyword)
        {
            chkUser.chkrights("USER");
            Session["USERNAME"] = Varible.User.USERNAME;
            Session["CustomerId"] = Varible.User.CustomerId;
            Session["ROLE"] = Varible.User.MEMBER_ID;
            var Data = LogDao.Instance.Log();
            var count = Data.Count.ToString();
            TempData["data"] = Data.ToList().ToPagedList(page ?? 1, 10);
            Session["Count"] = count;
            ViewBag.Count = Session["Count"];
            return View(TempData["data"]);
        }
    }
}