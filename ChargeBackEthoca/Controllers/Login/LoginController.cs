﻿using CaptchaMvc.HtmlHelpers;
using ChargeBackEthoca.Models.Component;
using ChargeBackEthoca.Models.Dao;
using ChargeBackEthoca.Models.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace ChargeBackEthoca.Controllers.Login
{
    public class LoginController : Controller
    {
        // GET: User
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(UserDto model)
        {
            if (this.IsCaptchaValid("Captcha is not valid"))
            {
                try
                {
                    string pUName = string.Empty;
                    string password = string.Empty;
                    int member_id = 0;
                    string pUid = model.USERNAME.ToString();
                    string pAss = model.PASSWORD.ToString();
                    string CusId = string.Empty;
                    string TimeZone = string.Empty;
                    if (pUid == "" || pUid == null)
                    {
                        ViewBag.Message = "Username or Password Incorrect";
                        return View();
                    }
                    if (pAss == "" || pAss == null)
                    {
                        ViewBag.Message = "Username or Password Incorrect";
                        return View();
                    }
                    if (UserDao.Instance.ChkAuthDB(pUid, ref member_id, ref pUName, ref password, ref CusId, ref TimeZone))
                    {
                        EUser user = new EUser();
                        user.USERID = pUid;
                        Varible.User = user;
                        Varible.User.USERNAME = pUid;
                        Varible.User.MEMBER_ID = member_id;
                        Varible.User.PASS = pAss;
                        Varible.User.CustomerId = CusId;
                        Varible.User.TimeZone = TimeZone;

                        //เช็ค password ถูกหรือไม่
                        string pass = DataCryptography.Decrypt(password.ToString().Trim());
                        if (model.PASSWORD != pass)
                        {
                            ViewBag.Message = "Username or Password Incorrect";
                            TempData["Message"] = ViewBag.Message;
                            return View();
                        }
                        else if (member_id != Varible.User.MEMBER_ID)
                        {
                            ViewBag.Message = "Username or Password Incorrect";
                            TempData["Message"] = ViewBag.Message;
                            return View();
                        }
                    }
                    else
                    {
                        ViewBag.Message = "Username or Password Incorrect";
                        TempData["Message"] = ViewBag.Message;

                    }
                    return RedirectToAction("Index", "Transaction");
                }
                catch (Exception e)
                {
                    ViewBag.Message = "Error : " + e.Message;
                    return View();
                }
            }
            ViewBag.ErrMessage = "Error: captcha is not valid.";
            return View();
        }
        [HttpPost]
        public ActionResult Register(UserDto model)
        {
            try
            {
                string result = UserDao.Instance.Regis(model, "INS");
                if (result != "OK")
                {
                    ModelState.Clear();
                    ViewBag.Message = "Error";
                    TempData["Message"] = ViewBag.Message;
                }
                else
                {
                    ViewBag.Message = "Successfully";
                    TempData["Message"] = ViewBag.Message;
                }
            }
            catch
            {
                return RedirectToAction("Index", "Error");
            }
            return RedirectToAction("Index", "Login");
        }


        public ActionResult ChangePass()
        {
            return View();
        }
        public ActionResult ForgetPass(string Email)
        {
            try
            {
                var data = UserDao.Instance.GetEMail(Email);
                if (data != "OK")
                {
                    ViewBag.Message = "Username or email Is not exist";
                }
                else
                {
                    ViewBag.Message = "Successfully";
                }
            }
            catch
            {
                RedirectToAction("Index", "Error");
            }
            return View();
        }

        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();
            return RedirectToAction("Index", "Login");
        }
    }
}