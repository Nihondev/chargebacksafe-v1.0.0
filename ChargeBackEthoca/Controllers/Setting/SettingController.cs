﻿using ChargeBackEthoca.Models.Component;
using ChargeBackEthoca.Models.Dao;
using ChargeBackEthoca.Models.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ChargeBackEthoca.Controllers.Setting
{
    public class SettingController : Controller
    {
        // GET: Setting
        Permisstion chkUser = new Permisstion();
        
        public ActionResult Index()
        {
            chkUser.chkrights("USER");
            //app.TimeZomeCollection();
            //app.getCurrentDateTimeWithTimeZone("UTC-11");
            var UID = Varible.User.USERNAME;
            var Data = ProfileDao.Instance.GetDataUser().Find(smodel => smodel.USERNAME == UID);
            ViewBag.OldPass = Varible.User.PASS;
            ViewBag.ListZoneTime = SettingDao.Instance.TimeZomeCollection();
            return View(Data);
        }
        public ActionResult SetUpTimeZone(SetiingDto model)
        {
            try
            {
                SettingDao.Instance.TimeZone(model, "SetUp");
                return Json("Success Fully !");
            }
            catch
            {
                return Json("Error");
            }
        }
    }
}