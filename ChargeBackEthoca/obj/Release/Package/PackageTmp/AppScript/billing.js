﻿
function ReqBil() {
    var BillingDto = {}
    URL = 'https://121.40.228.193/Stripe/Billing';
    BillingDto.ReturnUrl = URL;
    BillingDto.CustomerId = $("#customer").val();
    $.ajax({
        type: 'POST',
        url: '/api/Payment/BillingPortal',
        data: JSON.stringify(BillingDto),
        contentType: 'application/json; charset=UTF-8',
        success: function (response) {
            console.log(response.url)
            $('#content_url').html(response.url);
            $('#bil').modal('show');
        },
        error: function () {
            alert("Api has error please try again !!!");
        }
    });
}

function ApiReq(status) {
    $('#reponse_billing').empty();
    var CustomerId = $("#customer").val();
    var Status = status;
    alert(status)
    $.ajax({
        type: 'GET',
        url: '/api/Payment/GetBilling',
        data: { CustomerId: CustomerId, Status, Status },
        contentType: 'application/json; charset=UTF-8',
        success: function (response) {
            if (response.data == undefined) {
                alert("No data" + ": " + Status + "!!!");
                $('#reponse_billing').append(
                    '<tr>' +
                    '<td colspan="19" class="text-center"><span>No Data !!!</span></td>' +
                    '</tr>'
                );
            }
            else {
                for (var i = 0; i < response.data.length; i++) {
                    $('#reponse_billing').append(
                        '<tr>' +
                        '<td><input type="button" onclick="ReqBil()" value="Manage" class="btn btn-primary"></td>' +
                        '<td>' + response.data[i].object + '</td>' +
                        '<td>' + response.data[i].account_country + '</td>' +
                        '<td>' + response.data[i].account_name + '</td>' +
                        '<td>' + "$" + (response.data[i].amount_due / 100).toFixed(2) + '</td>' +
                        '<td>' + "$" + (response.data[i].amount_paid / 100).toFixed(2) + '</td>' +
                        '<td>' + response.data[i].currency + '</td>' +
                        '<td>' + new Date(response.data[i].created) + '</td>' +
                        '<td>' + response.data[i].amount_remaining + '</td>' +
                        '<td>' + response.data[i].application_fee_amount + '</td>' +
                        '<td>' + response.data[i].attempt_count + '</td>' +
                        '<td>' + response.data[i].attempted + '</td>' +
                        '<td>' + response.data[i].auto_advance + '</td>' +
                        '<td>' + response.data[i].billing_reason + '</td>' +
                        '<td>' + response.data[i].charge + '</td>' +
                        '<td>' + response.data[i].collection_method + '</td>' +
                        '<td>' + response.data[i].customer + '</td>' +
                        '<td>' + response.data[i].customer_address + '</td>' +
                        '<td>' + response.data[i].customer_email + '</td>' +
                        '</tr>'
                    );
                }
            }
        },
        error: function () {
            alert("API has error please try again !!!");
        }
    });
}

// Add remove loading class on body element depending on Ajax request status
$(document).on({
    ajaxStart: function () {
        $("body").addClass("loading");
    },
    ajaxStop: function () {
        $("body").removeClass("loading");
    }
});